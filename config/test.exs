use Mix.Config

# Configure your database
config :tournament, Tournament.Repo,
  username: System.get_env("POSTGRES_USER") || "tournament",
  password: System.get_env("POSTGRES_PASSWORD") || "tournament",
  database: System.get_env("POSTGRES_DB") || "tournament_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :tournament, TournamentWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
